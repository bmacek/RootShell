#include "RootShell/common/RootFile.h"
#include "RootShell/common/RootObject.h"

#include "CatShell/basics/CatMessage.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootFile, CAT_NAME_ROOT_FILE, CAT_TYPE_ROOT_FILE);

RootFile::RootFile()
: _file(nullptr)
{

}

RootFile::~RootFile()
{

}

void RootFile::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_name && object.isSomething() && _dynamic)
        {
    		// new file

            CatPointer<CatMessage>   data = object.unsafeCast<CatMessage>();
            // close if it exists already
            if(_file)
            {
            	_file->Close();
            	delete _file;

                _objects_in_file.clear();
            }
            // open a new one
            _file = new TFile((data->message() + ".root").c_str(), "UPDATE");
            if(_file->IsZombie())
            {
            	ERROR(logger, "File " << data->message() << ".root failed to open.")
            	delete _file;
            	_file = nullptr;
            }
            LOG(logger, "File " << data->message() << ".root opened.")

	        if(flush)
	        {
	        	// close the file
	            if(_file)
	            {
	            	_file->Close();
	            	delete _file;
	            	_file = nullptr;

                    _objects_in_file.clear();
	            }
	        }
        }
        if(port == _in_port_save && object.isSomething())
        {
        	// write object
        	if(_file)
        	{
                _objects_in_file.push_back(object);

            	CatPointer<RootObject>   data = object.unsafeCast<RootObject>();

            	data->save(_file);
            	INFO(logger, "Object saved to file.")
        	}
            else
            {
                WARNING(logger, "Object not saved, because file not ready.");
            }
        }

        Algorithm::process_data(port, object, flush, logger);

}

void RootFile::algorithm_init()
{
        Algorithm::algorithm_init();

        // check if dynamic
        _dynamic = algo_settings().get_setting<bool>("dynamic", false);

        if(_dynamic)
        {
            _in_port_name = declare_input_port("in_file_name", CAT_NAME_MESSAGE);
            _file = nullptr;
        }
        else
        {
            // static
            _name = algo_settings().get_setting<std::string>("name", "out.root");
            // open a new one
            _file = new TFile(_name.c_str(), "UPDATE");
            if(_file->IsZombie())
            {
                delete _file;
                _file = nullptr;
            }
        }
        _in_port_save = declare_input_port("in_root_object", CAT_NAME_ROOT_OBJECT);
}

void RootFile::algorithm_deinit()
{
        if(_file)
        {
        	_file->Close();
        	delete _file;
        	_file = nullptr;

            _objects_in_file.clear();
        }

        Algorithm::algorithm_deinit();
}

} // namespace RootShell
