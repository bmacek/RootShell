#include "CatShell/core/Plugin.h"
#include "RootShell/common/RootObject.h"
#include "RootShell/common/RootFile.h"
#include "RootShell/common/RootGraph.h"
#include "RootShell/common/RootGraphDraw.h"
#include "RootShell/common/RootTree.h"
#include "RootShell/common/RootTreeDraw.h"
#include "RootShell/common/RootHistogram.h"
#include "RootShell/common/RootHistogramDraw.h"

using namespace std;

namespace RootShell {

EXPORT_PLUGIN(CatShell::Plugin, RootCommonPlugin)
START_EXPORT_CLASSES(RootCommonPlugin)
EXPORT_VIRTUAL_CLASS(RootObject, CAT_NAME_ROOT_OBJECT, RootCommonPlugin)
EXPORT_CLASS(RootFile, CAT_NAME_ROOT_FILE, RootCommonPlugin)
EXPORT_CLASS(RootGraph, CAT_NAME_ROOT_GRAPH, RootCommonPlugin)
EXPORT_CLASS(RootGraphDraw, CAT_NAME_ROOT_GRAPHDRAW, RootCommonPlugin)
EXPORT_CLASS(RootTree, CAT_NAME_ROOT_TREE, RootCommonPlugin)
EXPORT_CLASS(RootTreeDraw, CAT_NAME_ROOT_TREEDRAW, RootCommonPlugin)
EXPORT_CLASS(RootHistogram, CAT_NAME_ROOT_HISTOGRAM, RootCommonPlugin)
EXPORT_CLASS(RootHistogramDraw, CAT_NAME_ROOT_HISTOGRAMDRAW, RootCommonPlugin)
END_EXPORT_CLASSES(RootCommonPlugin)

} // namespace RootShell
