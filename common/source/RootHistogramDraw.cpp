#include "RootShell/common/RootHistogramDraw.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootHistogramDraw, CAT_NAME_ROOT_HISTOGRAMDRAW, CAT_TYPE_ROOT_HISTOGRAMDRAW);

RootHistogramDraw::RootHistogramDraw()
{

}

RootHistogramDraw::~RootHistogramDraw()
{

}

void RootHistogramDraw::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        char text[100];
        if(port == _in_port_object && object.isSomething())
        {
                _histogram->fill(_value.get_value(object));
        }

        if(flush)
        {
                sprintf(text, "Publishing existing histogram and creating new one.");
                LOG(logger, text);
                publish_data(_out_port_histogram, _histogram, logger);
                // create new histogram
                _histogram = _pool->get_element();
                _histogram->define_histogram(_name, _title, _nbins, _x_min, _x_max);
                _histogram->reset();
        }

        Algorithm::process_data(port, object, flush, logger);

}

void RootHistogramDraw::algorithm_init()
{
        Algorithm::algorithm_init();

        // get the pool
        _pool = PluginDeamon::get_pool<RootHistogram>();

        // prepare the ports
        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_histogram = declare_output_port("out_histogram", CAT_NAME_ROOT_HISTOGRAM);

        // get the settings
        _name = algo_settings().get_setting<std::string>("name", "HistogramName");
        _title = algo_settings().get_setting<std::string>("title", "HistogramTitle");
        _nbins = algo_settings().get_setting<std::uint32_t>("bins", 1);
        _x_min = algo_settings().get_setting<double>("x_min", 0);
        _x_max = algo_settings().get_setting<double>("x_max", 1);

	_value.define(algo_settings()["value"]);


        // create a histogram
        _histogram = _pool->get_element();
        _histogram->define_histogram(_name, _title, _nbins, _x_min, _x_max);
	
}

void RootHistogramDraw::algorithm_deinit()
{
        _histogram.make_nullptr();

        Algorithm::algorithm_deinit();
}

} // namespace RootShell
