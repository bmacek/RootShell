#include "RootShell/common/RootTreeDraw.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootTreeDraw, CAT_NAME_ROOT_TREEDRAW, CAT_TYPE_ROOT_TREEDRAW);

RootTreeDraw::RootTreeDraw()
{

}

RootTreeDraw::~RootTreeDraw()
{

}

void RootTreeDraw::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        char text[100];
        if((port >= _in_port_object) && (port < _in_port_object+_ports) && object.isSomething())
        {
                // calculate the port number
                uint16_t p;
                p = port - _in_port_object;

                _tree->fill(p, object);
        }

        if(port == _in_port_object && flush)
        {
                LOG(logger, "Publishing existing tree and creating new one.");
                publish_data(_out_port_tree, _tree, logger);
                // create new graph
                _tree = _pool->get_element();
                _tree->set_name(algo_settings().get_setting<std::string>("name", "my_tree"));

                // create branches
                _ports = 0;
                for(auto& sett: algo_settings())
                {
                        if(sett.name() == "branches")
                        {
                                // found the branch description
                                for(auto& s: sett)
                                {
                                        _tree->add_branch(_ports, s.name(), s.get_setting<std::string>("type", "?"), s);
                                }

                                // go to the next port
                                ++_ports;
                        }
                }
        }

        Algorithm::process_data(port, object, flush, logger);
}

void RootTreeDraw::algorithm_init()
{
        Algorithm::algorithm_init();

        // get the pool
        _pool = PluginDeamon::get_pool<RootTree>();

        // create a tree
        _tree = _pool->get_element();
        _tree->set_name(algo_settings().get_setting<std::string>("name", "my_tree"));

        // create branches
        _ports = 0;
        for(auto& sett: algo_settings())
        {
                if(sett.name() == "branches")
                {
                        // found the branch description
                        for(auto& s: sett)
                        {
                                _tree->add_branch(_ports, s.name(), s.get_setting<std::string>("type", "?"), s);
                        }

                        // go to the next port
                        ++_ports;
                }
        }

        // prepare the ports
        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT, _ports);
        _out_port_tree = declare_output_port("out_tree", CAT_NAME_OBJECT);
}

void RootTreeDraw::algorithm_deinit()
{
        _tree.make_nullptr();

        Algorithm::algorithm_deinit();
}

} // namespace RootShell
