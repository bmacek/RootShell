#include "RootShell/common/RootGraph.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootGraph, CAT_NAME_ROOT_GRAPH, CAT_TYPE_ROOT_GRAPH);

RootGraph::RootGraph()
{

}

RootGraph::~RootGraph()
{

}

void RootGraph::set_name_title(const std::string& name, const std::string& title)
{
        _graph.SetTitle(title.c_str());
        _graph.SetName(name.c_str());
}

void RootGraph::add_point(double x, double y, double err_x, double err_y)
{
        int i= _graph.GetN();
        //_graph.Set(i+1);
        _graph.SetPoint(i, x, y); 
        _graph.SetPointError(i, err_x, err_y);
}

void RootGraph::save(TFile* file) const
{
        file->cd();
        _graph.Write();
}

} // namespace RootShell
