#include "RootShell/common/RootGraphDraw.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootGraphDraw, CAT_NAME_ROOT_GRAPHDRAW, CAT_TYPE_ROOT_GRAPHDRAW);

RootGraphDraw::RootGraphDraw()
{

}

RootGraphDraw::~RootGraphDraw()
{

}

void RootGraphDraw::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        char text[100];
        if(port == _in_port_object && object.isSomething())
        {
                double x,y,x_err,y_err;
                x = _element_x.get_value(object);
                x_err = _element_x_err.get_value(object);
                y = _element_y.get_value(object);
                y_err = _element_y_err.get_value(object);

                _graph->add_point(x, y, x_err, y_err);

                //sprintf(text, "Adding point: %f+-%f, %f+-%f (%u)", x, x_err, y, y_err, _graph->get_number_of_points());
                //LOG(logger, text);
        }

        if(flush)
        {
                sprintf(text, "Publishing existing graph with %u points and creating new one.", _graph->get_number_of_points());
                LOG(logger, text);
                publish_data(_out_port_graph, _graph, logger);
                // create new graph
                _graph = _pool->get_element();
                _graph->set_name_title(_name, _title);
                _graph->reset();
        }

        Algorithm::process_data(port, object, flush, logger);

}

void RootGraphDraw::algorithm_init()
{
        Algorithm::algorithm_init();

        // get the pool
        _pool = PluginDeamon::get_pool<RootGraph>();

        // prepare the ports
        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_graph = declare_output_port("out_graph", CAT_NAME_OBJECT);

        // get the settings
        _name = algo_settings().get_setting<std::string>("name", "GraphName");
        _title = algo_settings().get_setting<std::string>("title", "GraphTitle");

        // define the data elements
        _element_x.define(algo_settings()["variable_x"]);
        _element_x_err.define(algo_settings()["variable_x_err"]);
        _element_y.define(algo_settings()["variable_y"]);
        _element_y_err.define(algo_settings()["variable_y_err"]);

        // create a graph
        _graph = _pool->get_element();
        _graph->set_name_title(_name, _title);
}

void RootGraphDraw::algorithm_deinit()
{
        _graph.make_nullptr();

        Algorithm::algorithm_deinit();
}

} // namespace RootShell
