#include "RootShell/common/RootObject.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootObject, CAT_NAME_ROOT_OBJECT, CAT_TYPE_ROOT_OBJECT);

RootObject::RootObject()
{

}

RootObject::~RootObject()
{

}

} // namespace RootShell
