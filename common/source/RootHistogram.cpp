#include "RootShell/common/RootHistogram.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootHistogram, CAT_NAME_ROOT_HISTOGRAM, CAT_TYPE_ROOT_HISTOGRAM);

RootHistogram::RootHistogram()
{
	_histogram = nullptr;
}

RootHistogram::~RootHistogram()
{
	if(_histogram != nullptr) delete _histogram;
}

void RootHistogram::define_histogram(const std::string& name, const std::string& title, std::uint32_t nbins, double x_min, double x_max)
{
	if(_histogram == nullptr)
		_histogram = new TH1F;
	_histogram->SetName(name.c_str());
	_histogram->SetTitle(title.c_str());	
	_histogram->SetBins(nbins, x_min, x_max);
}

void RootHistogram::fill(float value)
{
	_histogram->Fill(value);
}

void RootHistogram::save(TFile* file) const
{
        file->cd();
        _histogram->Write();
}

} // namespace RootShell
