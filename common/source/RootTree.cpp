#include "RootShell/common/RootTree.h"

using namespace std;
using namespace CatShell;

namespace RootShell {

CAT_OBJECT_IMPLEMENT(RootTree, CAT_NAME_ROOT_TREE, CAT_TYPE_ROOT_TREE);

RootTree::RootTree()
{
    _tree = new TTree;
}

RootTree::~RootTree()
{
	for(auto& x: _branches)
	{
        if(x.type == "std::uint8_t")
        {
            delete_variable<std::uint8_t>(x);
        }
        else if(x.type == "std::uint16_t")
        {
            delete_variable<std::uint16_t>(x);
        }
        else if(x.type == "std::uint32_t")
        {
            delete_variable<std::uint32_t>(x);
        }
        else if(x.type == "float")
        {
            delete_variable<float>(x);
        }
        else if(x.type == "double")
        {
            delete_variable<double>(x);
        }
        else if(x.type == "bool")
        {
            delete_variable<bool>(x);
        }
        delete x.element;
    }

    delete _tree;
}

void RootTree::save(TFile* file) const
{
        file->cd();
        _tree->Write();
}

void RootTree::fill(uint16_t branch_group, CatPointer<CatObject> object)
{
	for(tBranch& br: _branches)
	{
        // only branches for this group
        if(br.group == branch_group)
        {
    		// for each branch fill the variables
            if(br.type == "bool")
            {
                br.element->fill(br.variable, object);    
            } 
            else
                br.element->fill(br.variable, object);           
            br.filled = true;
        }
	}

    // check if all are true
    for(tBranch& br: _branches)
    {
        if(!br.filled)
            return;
    }   

    // all are filled
	// fill the ROOT tree
	_tree->Fill();

    // remove the flags
    for(tBranch& br: _branches)
        br.filled = false;
}

int RootTree::add_branch(uint16_t branch_group, const std::string& name, const std::string& type, Setting& setting)
{
        tBranch tmp;
        tmp.name = name;
        tmp.type = type;
        if(type.substr(0,12) == "std::uint8_t")// test if it starts with ...
        {
            populate_branch<std::uint8_t>(branch_group, tmp, name, setting, "/b");
        }
        else if(type.substr(0,13) == "std::uint16_t")
        {
            populate_branch<std::uint16_t>(branch_group, tmp, name, setting, "/s");
        }
        else if(type.substr(0,13) == "std::uint32_t")
        {
            populate_branch<std::uint32_t>(branch_group, tmp, name, setting, "/i");
        }
        else if(type.substr(0,5) == "float")
        {
            populate_branch<float>(branch_group, tmp, name, setting, "/F");
        }
        else if(type.substr(0,6) == "double")
        {
            populate_branch<double>(branch_group, tmp, name, setting, "/D");
        }
        else if(type.substr(0,4) == "bool")
        {
            populate_branch<bool>(branch_group, tmp, name, setting, "/O");
        }
        else
                return -1;

        _branches.push_back(tmp);
        return _branches.size()-1;
}

} // namespace RootShell
