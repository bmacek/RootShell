#ifndef INCLUDE_ROOT_ROOTTREE_INCLUDED
#define INCLUDE_ROOT_ROOTTREE_INCLUDED

#include "CatShell/core/Setting.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatElement.h"

#include "RootShell/common/defines.h"
#include "RootShell/common/RootObject.h"

#include "TTree.h"

namespace RootShell {

class RootTree : public RootObject
{
        CAT_OBJECT_DECLARE(RootTree, RootObject, CAT_NAME_ROOT_TREE, CAT_TYPE_ROOT_TREE);
public:

        RootTree();
                /// Constructor: default.

        virtual ~RootTree();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void save(TFile* file) const;
                /// Should save the object to file.
//----------------------------------------------------------------------

        void set_name(const std::string& name);
                /// Set the name of the tree.

        int add_branch(uint16_t branch_group, const std::string& name, const std::string& type, CatShell::Setting& setting);
                /// Returns the index of the branch. If negative number is returned the branch creation failed.

        void fill(uint16_t branch_group, CatShell::CatPointer<CatShell::CatObject> object);
                /// Fills the tree with the current data.

protected:
private:
        typedef struct {
                std::uint16_t group;
                bool filled;
                std::string name;
                std::string type;
                void*       variable;
                CatShell::CatVirtualElement* element;
        } tBranch;


        template<class T>
        void populate_branch(uint16_t branch_group, tBranch& tmp, const std::string& name, CatShell::Setting& setting, const std::string& marker);
                /// Helper function for preparing branches.

        template<class T>
        void delete_variable(tBranch& x);
                /// Helper function for cleaning the variables.

private:

        TTree*                  _tree;
        std::vector<tBranch>    _branches;
};

inline StreamSize RootTree::cat_stream_get_size() const { return RootObject::cat_stream_get_size(); }
inline void RootTree::cat_stream_out(std::ostream& output) { return RootObject::cat_stream_out(output); }
inline void RootTree::cat_stream_in(std::istream& input) { return RootObject::cat_stream_in(input); }
inline void RootTree::cat_print(std::ostream& output, const std::string& padding) { RootObject::cat_print(output, padding); }
inline void RootTree::cat_free() { _tree->Reset(); } //if(_tree) delete _tree; _tree=nullptr; }
inline void RootTree::set_name(const std::string& name) { _tree->SetName(name.c_str()); } 

template<class T>
void RootTree::populate_branch(uint16_t branch_group, tBranch& tmp, const std::string& name, CatShell::Setting& setting, const std::string& marker)
{
        tmp.element = new CatShell::CatElement<T>(setting);
        tmp.type = tmp.element->get_type();
        tmp.group = branch_group;
        tmp.filled = false;
        if(tmp.element->has_dimension())
        {
                tmp.variable = new T[tmp.element->get_dimension()];
                char br_name[1000];
                sprintf(br_name, "%s[%u]%s", name.c_str(), tmp.element->get_dimension(), marker.c_str());
                _tree->Branch(name.c_str(), tmp.variable, br_name);
        }
        else
        {
                tmp.variable = new T;
                _tree->Branch(name.c_str(), tmp.variable, (name+marker).c_str());
        }
}

template<class T>
void RootTree::delete_variable(tBranch& x)
{
    if(x.element->has_dimension())
        delete [] ((T*)x.variable);
    else
        delete ((T*)x.variable);
}

} // namespace RootShell

#endif
