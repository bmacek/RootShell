#ifndef INCLUDE_ROOT_ROOTOBJECT_INCLUDED
#define INCLUDE_ROOT_ROOTOBJECT_INCLUDED

#include "CatShell/core/CatObject.h"

#include "RootShell/common/defines.h"

#include "TFile.h"

namespace RootShell {

class RootObject : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(RootObject, CatShell::CatObject, CAT_NAME_ROOT_OBJECT, CAT_TYPE_ROOT_OBJECT);
public:

        RootObject();
                /// Constructor: default.

        virtual ~RootObject();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void save(TFile* file) const;
                /// Should save the object to file.
//----------------------------------------------------------------------

protected:
private:
};

inline StreamSize RootObject::cat_stream_get_size() const { return 0; }
inline void RootObject::cat_stream_out(std::ostream& output) { }
inline void RootObject::cat_stream_in(std::istream& input) { }
inline void RootObject::cat_print(std::ostream& output, const std::string& padding) { CatShell::CatObject::cat_print(output, padding); }
inline void RootObject::cat_free() { }
inline void RootObject::save(TFile* file) const { }

} // namespace RootShell

#endif
