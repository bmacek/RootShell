#ifndef INCLUDE_ROOT_ROOTHISTOGRAM_INCLUDED
#define INCLUDE_ROOT_ROOTHISTOGRAM_INCLUDED

#include "CatShell/core/CatObject.h"

#include "RootShell/common/defines.h"
#include "RootShell/common/RootObject.h"

#include "TH1F.h"

namespace RootShell {

class RootHistogram : public RootObject
{
        CAT_OBJECT_DECLARE(RootHistogram, RootObject, CAT_NAME_ROOT_HISTOGRAM, CAT_TYPE_ROOT_HISTOGRAM);
public:

        RootHistogram();
                /// Constructor: default.

        virtual ~RootHistogram();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void save(TFile* file) const;
                /// Should save the object to file.
//----------------------------------------------------------------------

        void reset();
                /// Resets the content of the histogram.

	void define_histogram(const std::string& name, const std::string& title, std::uint32_t nbins, double x_min, double x_max);
		/// Sets the name, title and number of bins from 'x_min' to x_max'.

        void fill(float value);
                /// Fill histogram with value.

        std::uint32_t get_number_of_bins() const;
                /// Returns the number of bins in the histogram.

protected:
private:

        TH1F*   _histogram;
};

inline StreamSize RootHistogram::cat_stream_get_size() const { return RootObject::cat_stream_get_size(); }
inline void RootHistogram::cat_stream_out(std::ostream& output) { return RootObject::cat_stream_out(output); }
inline void RootHistogram::cat_stream_in(std::istream& input) { return RootObject::cat_stream_in(input); }
inline void RootHistogram::cat_print(std::ostream& output, const std::string& padding) { RootObject::cat_print(output, padding); }
inline void RootHistogram::cat_free() { }
inline std::uint32_t RootHistogram::get_number_of_bins() const { return _histogram->GetNbinsX(); }
inline void RootHistogram::reset() { _histogram->Reset(); }

} // namespace RootShell

#endif
