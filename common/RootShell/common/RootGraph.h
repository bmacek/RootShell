#ifndef INCLUDE_ROOT_ROOTGRAPH_INCLUDED
#define INCLUDE_ROOT_ROOTGRAPH_INCLUDED

#include "CatShell/core/CatObject.h"

#include "RootShell/common/defines.h"
#include "RootShell/common/RootObject.h"

#include "TGraphErrors.h"

namespace RootShell {

class RootGraph : public RootObject
{
        CAT_OBJECT_DECLARE(RootGraph, RootObject, CAT_NAME_ROOT_GRAPH, CAT_TYPE_ROOT_GRAPH);
public:

        RootGraph();
                /// Constructor: default.

        virtual ~RootGraph();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void save(TFile* file) const;
                /// Should save the object to file.
//----------------------------------------------------------------------

        void reset();
                /// Resets the content of the graph.

        void set_name_title(const std::string& name, const std::string& title);
                /// Sets the name and the title of the graph.

        void add_point(double x, double y, double err_x, double err_y);
                /// Insert a new point into graph.

        std::uint32_t get_number_of_points() const;
                /// Returns the number of data points in the graph.

protected:
private:

        TGraphErrors   _graph;
};

inline StreamSize RootGraph::cat_stream_get_size() const { return RootObject::cat_stream_get_size(); }
inline void RootGraph::cat_stream_out(std::ostream& output) { return RootObject::cat_stream_out(output); }
inline void RootGraph::cat_stream_in(std::istream& input) { return RootObject::cat_stream_in(input); }
inline void RootGraph::cat_print(std::ostream& output, const std::string& padding) { RootObject::cat_print(output, padding); }
inline void RootGraph::cat_free() { }
inline std::uint32_t RootGraph::get_number_of_points() const { return _graph.GetN(); }
inline void RootGraph::reset() { _graph.Set(0); }

} // namespace RootShell

#endif
