#ifndef INCLUDE_ROOT_ROOTHISTOGRAMDRAW_INCLUDED
#define INCLUDE_ROOT_ROOTHISTOGRAMDRAW_INCLUDED

#include "CatShell/core/CatPointer.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/CatElement.h"
#include "CatShell/core/MemoryPool.h"

#include "RootShell/common/defines.h"
#include "RootShell/common/RootHistogram.h"

namespace RootShell {

class RootHistogramDraw : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(RootHistogramDraw, CatShell::Algorithm, CAT_NAME_ROOT_HISTOGRAMDRAW, CAT_TYPE_ROOT_HISTOGRAMDRAW);
public:

        RootHistogramDraw();
                /// Constructor: default.

        virtual ~RootHistogramDraw();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        CatShell::Algorithm::DataPort   _in_port_object;
        CatShell::Algorithm::DataPort   _out_port_histogram;

        std::string     _name;
        std::string     _title;

        CatShell::CatPointer<RootHistogram>  _histogram;

        std::uint32_t                           _nbins;
        double                                  _x_min;
        double                                  _x_max;
	CatShell::CatElement<float>             _value;

        CatShell::MemoryPool<RootHistogram>*    _pool;
};

inline StreamSize RootHistogramDraw::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void RootHistogramDraw::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void RootHistogramDraw::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void RootHistogramDraw::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void RootHistogramDraw::cat_free() { _histogram.make_nullptr(); }

} // namespace RootShell

#endif
