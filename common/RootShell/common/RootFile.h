#ifndef INCLUDE_ROOT_ROOTFILE_INCLUDED
#define INCLUDE_ROOT_ROOTFILE_INCLUDED

#include "CatShell/core/Algorithm.h"

#include "RootShell/common/defines.h"

#include "TFile.h"

namespace RootShell {

class RootFile : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(RootFile, CatShell::Algorithm, CAT_NAME_ROOT_FILE, CAT_TYPE_ROOT_FILE);
public:

        RootFile();
                /// Constructor: default.

        virtual ~RootFile();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        CatShell::Algorithm::DataPort   _in_port_name;
        CatShell::Algorithm::DataPort   _in_port_save;

        TFile*  _file;

        std::vector<CatShell::CatPointer<CatShell::CatObject>>  _objects_in_file;

        // settings
        bool _dynamic;
        std::string _name;
};

inline StreamSize RootFile::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void RootFile::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void RootFile::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void RootFile::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void RootFile::cat_free() { }

} // namespace RootShell

#endif
